# Proposal

## Facilitator information

### Your information

- First name

  Raniere

- Surname

  Silva

- Email address

  raniere@rgaiacs.com

- Organisation or affiliation

  Software Sustainability Institute

- What country do you currently reside in?

  United Kingdom

- Twitter handle

  @rgaiacs

- GitHub handle

  @rgaiacs

### Additional facilitator's information

- First name



- Surname



- Email address



- Organisation or affiliation



- What country do you currently reside in?



- Twitter handle



- GitHub handle

## Festival spaces

- What space do feel your session will best contribute to?

  Openness

- Is there an alternate space your session could contribute to?

  Privacy and Security

## Language of your session

- Do you wish to propose or deliver your session in another language?

  No

## Describe your session

- What is the name of your session?

  Open Community Metrics and Privacy

- What will happen in your session?

  We will discuss about that data should **and must not** be collected, aggregated and make public
  when doing community metric analyze.

- What is the goal or outcome of your session?

  The goal of this session is data gathering.
  As outcome we will write a blog post after MozFest
  based on the notes of the session.

- After the festival, how will you and your participants take the learning and activities forward?

  Outcomes of this session will be shared with the [CHAOSS](https://wiki.linuxfoundation.org/chaoss/) (Community Health Analytics OSS) Project.

- How will you deal with varying numbers of participants in your session? What if 30 participants attend? What if there are 3?

  As a data gathering session,
  we plan to have groups of 5-6 people
  that will recollect at the end of the session.
  This way we can work with what number of participants we end having.

## Session formats

- Choose from the formats below that most suits your session delivery

  Learning Forum

- How much time you will need?

  90 mins

## Additional support

- If your session requires additional materials or electronic equipment, please outline your needs.

  N/A

- Do you qualify yourself as such?

  No.

  
